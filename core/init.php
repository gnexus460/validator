<?php
session_start();

$GLOBALS['config'] = array(
    'mysql' => array(
        'host' => '127.0.0.1',
        'username' => 'laravel',
        'password' => '8tg6w7krp3',
        'db' => 'loginoop'
    ),
    'remember' => array(
        'cookie_name' => 'hash',
        'cookie_expiry' => 604800
    ),
    'session' => array(
        'session_name' => 'user'
    ),
);

spl_autoload_register(function ($class){
    require_once 'Classes/'.$class.'.php';
});

require_once 'functions/sanitize.php';