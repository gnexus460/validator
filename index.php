<?php
require_once("core/init.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</head>
<body>
	<div class="container">
        <?php
        $users = DB::getInstance()->query('Select * from users');
        if($users->error()) {
            echo 'No users';
        } else {
            foreach ($users->results() as $user) {
                dump($user->username);
            };
        }


            if(Input::exist()) {
                $validate = new Validation();
                $validation = $validate->validate($_POST, array(
                        'name' => array(
                                'required' => true,
                                'min' => 2,
                                'max' => 20,
                                'unique' => 'users',
                                'string' => true
                        ),
                        'email' => array(
                                'required' => true,
                                'min' => 6
                        ),
                        'phone' => array(
                                'required' => true,
                                'matches' => 'password'
                        ),
                ));


                if($validation->passed()) {
                    echo 'passed';
                } else {
                    print_r($validation->errors());
                }
            }
        ?>
		<div class="col-md-12">
            <h1>Форма</h1>
            <form action="" method="POST">
                <input type="text" name="name" placeholder="name" value="<?php echo Input::get('name')?>" class="form-control"><br />
                <input type="text" name="email" placeholder="email" class="form-control"><br />
                <input type="text" name="phone" placeholder="phone" class="form-control"><br />
                <input type="submit" class="btn btn-success">
            </form>
		</div>
	</div>
</body>
</html>